package zen_training_singleton;

public class Main {

	public static void main(String[] args) {
		ConnectionManager connectionManager = ConnectionManager.getInstance();
		
		connectionManager.openConnection();
		String connectionName = connectionManager.getConnectionName();
		System.out.println(connectionName);
		connectionManager.closeConnection();
	}

}
