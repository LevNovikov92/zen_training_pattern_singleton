package zen_training_singleton;

public class ConnectionManager {
	private static final ConnectionManager mInstance = new ConnectionManager();
	
	private ConnectionManager() {}
	
	public static ConnectionManager getInstance() {
		return mInstance;
	}
	
	public String getConnectionName() {
		return "Connection";
	}
	
	public void openConnection() {
		System.out.println("Opening connection...");
	}
	
	public void closeConnection() {
		System.out.println("Closing connection...");
	}
}
